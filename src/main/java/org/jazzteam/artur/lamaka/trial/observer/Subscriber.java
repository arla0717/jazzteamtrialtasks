package org.jazzteam.artur.lamaka.trial.observer;

interface Subscriber {
  void onReceive (byte[] rawData);
}