package org.jazzteam.artur.lamaka.trial.observer;

interface NotificationEngine {
  void subscribe(Subscriber o);
  void unsubscribe(Subscriber o);
  void notifySubscribers(byte[] data);
}
