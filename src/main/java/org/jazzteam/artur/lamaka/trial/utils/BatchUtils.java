package org.jazzteam.artur.lamaka.trial.utils;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.codepoetics.protonpack.Indexed;
import com.codepoetics.protonpack.StreamUtils;
import java.util.Collection;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class BatchUtils {


  /**
   * The method classify elements of <code>items</code> to the batches.
   * Note: If <code>items.size()</code> is not divisible by <code>batchSize</code> then last Collection will contain
   *       orphaned elements
   *
   * @param batchSize Size of batch. Should be not less than 1
   *
   *
   * @throws IllegalArgumentException if <code>batchSize</code> is less than 1
   *
   * */
  public static Collection<Collection<?>> getBatches(Collection<?> items, int batchSize) {

    if(batchSize < 1) {
      throw new IllegalArgumentException("batchSize must be greater than 0");
    }

    return
        // First we prepare List<Pair<Index, Item>>
        StreamUtils.zipWithIndex(items.stream())
            // Then we classify each pair to the on of Collection
            .collect(groupingBy(i -> i.getIndex() / batchSize))
            // Now we have a Map<Index, Collection<Pair<Index, Item>>
            .values().stream()
            // And finally we just unbox the Map to Collection<Collection<Item>>
            .map(batch ->
                batch.stream()
                    .map(Indexed::getValue)
                    .collect(toList())
            ).collect(Collectors.toList());
  }

}
