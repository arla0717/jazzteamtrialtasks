package org.jazzteam.artur.lamaka.trial.vfs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("WeakerAccess")
@Getter
@Setter(AccessLevel.PRIVATE)
@ToString(of = "name")
public class Node {

  private String name;
  private Node parent;
  private List<Node> children = new LinkedList<>();

  public Node(String name) {
    this.name = name;
  }

  /**
   * Note: If node already has a child with the same name, it will be overwritten
   */
  public void insertChild(Node node) {
    if (StringUtils.isEmpty(node.getName()) || StringUtils.isWhitespace(node.getName())) {
      throw new IllegalArgumentException("Node name must be specified");
    }

    // Remove child with the same name if exist
    children = children.stream()
        .filter(child -> !child.getName().equals(node.getName()))
        .collect(Collectors.toList());

    node.setParent(this);
    children.add(node);
  }

  public void deleteChild(Node node) {
    if (children.stream().noneMatch(child -> child == node)) {
      throw new IllegalArgumentException(
          String.format("Node %s doesn't contain child %s", this.getName(), node.getName()));
    }

    children.remove(node);
  }

  public void moveTo(Node node) {
    getParent().deleteChild(this);
    node.insertChild(this);
  }

  /**
   * Returns path from root to this node (exclusive)
   */
  public List<Node> getPath() {
    final List<Node> path = new ArrayList<>();
    Node current = this;

    while (current != null) {
      path.add(current);
      current = current.getParent();
    }

    Collections.reverse(path);
    return path;
  }

}

