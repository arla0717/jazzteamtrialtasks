package org.jazzteam.artur.lamaka.trial.observer;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class DataReceiver implements NotificationEngine {

  @Getter
  private List<Subscriber> subscribers = new ArrayList<>();

  @Override
  public void subscribe(Subscriber subscriber) {
    if(!subscribers.contains(subscriber)) {
      subscribers.add(subscriber);
    }
  }

  @Override
  public void unsubscribe(Subscriber subscriber) {
    subscribers.remove(subscriber);
  }

  @Override
  public void notifySubscribers(byte[] data) {
    subscribers.parallelStream()
        .forEach(subscriber -> subscriber.onReceive(data));
  }
}
