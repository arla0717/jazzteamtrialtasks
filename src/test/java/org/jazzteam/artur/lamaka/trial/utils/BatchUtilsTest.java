package org.jazzteam.artur.lamaka.trial.utils;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.jazzteam.artur.lamaka.trial.utils.BatchUtils.getBatches;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import org.junit.Test;

public class BatchUtilsTest {

  @Test
  public void shouldKeepItemOrderingAsIs() {
    final Collection<Collection<?>> result = getBatches(
        Arrays.asList("0", 1, "2", 3, "4", 5, "6", 7),
        4
    );

    final ArrayList<Collection<?>> resultList = new ArrayList<>(result);
    assertThat(resultList.get(0), contains("0", 1, "2", 3));
    assertThat(resultList.get(1), contains("4", 5, "6", 7));
  }

  @Test
  public void shouldReturnEqualSizeCollectionsIfItemsAreEvenlyDivisibleByBatchSize() {
    final Collection<Collection<?>> result = getBatches(
        Arrays.asList("0", 1, new Object(), 3, 4, 5, 6, 7),
        4
    );

    assertThat(result, hasSize(2));
    assertThat(new ArrayList<>(result).get(0), hasSize(4));
    assertThat(new ArrayList<>(result).get(1), hasSize(4));
  }

  @Test
  public void shouldReturnSmallLastCollectionIfItemsAreNotEvenlyDivisibleByBatchSize() {
    final Collection<Collection<?>> result = getBatches(
        Arrays.asList("0", 1, new Object(), 3, 4, 5, 6, 7, "8", "9"),
        3
    );

    assertThat(new ArrayList<>(result).get(3), contains("9"));
  }

  @Test
  public void shouldReturnOnlyOneCollectionIfBatchSizeIsGreaterThanItemsSize() {
    final Collection<Collection<?>> result = getBatches(
        Arrays.asList(1, 2, 3),
        Integer.MAX_VALUE
    );

    assertThat(result, hasSize(1));
  }

  @Test
  public void shouldReturnEmptyCollectionIfItemsAreEmpty() {
    final Collection<Collection<?>> result = getBatches(
        Collections.emptyList(),
        Integer.MAX_VALUE
    );

    assertThat(result, hasSize(0));
  }

  @Test
  public void shouldAcceptNullAsItem() {
    final Collection<Collection<?>> result = getBatches(
        Arrays.asList(null, 1, null, 2, null, 3),
        Integer.MAX_VALUE
    );
    final ArrayList<Collection<?>> resultList = new ArrayList<>(result);

    assertThat(result, hasSize(1));
    assertThat(resultList.get(0), contains(null, 1, null, 2, null, 3));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionIfBatchSizeIsLessThanOne() {
    getBatches(
        Arrays.asList(null, 1, null, 2, null, 3),
        0
    );
  }


}