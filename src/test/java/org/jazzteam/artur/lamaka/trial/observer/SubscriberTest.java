package org.jazzteam.artur.lamaka.trial.observer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.junit.Before;
import org.junit.Test;

public class SubscriberTest {

  @Getter
  class GreenSubscriber implements Subscriber {

    private List<byte[]> notifications = new ArrayList<>();

    @Override
    public void onReceive(byte[] rawData) {
      notifications.add(rawData);
    }
  }

  @Getter
  class RedSubscriber implements Subscriber {

    private List<byte[]> notifications = new ArrayList<>();

    @Override
    public void onReceive(byte[] rawData) {
      notifications.add(rawData);
    }
  }

  private DataReceiver dataReceiver;
  private RedSubscriber redSubscriber;
  private GreenSubscriber greenSubscriber;

  @Before
  public void setUp() {
    this.dataReceiver = new DataReceiver();
    this.redSubscriber = new RedSubscriber();
    this.greenSubscriber = new GreenSubscriber();
  }

  @Test
  public void shouldSubscribeToDataReceiver() {
    dataReceiver.subscribe(redSubscriber);
    dataReceiver.subscribe(greenSubscriber);

    assertThat(dataReceiver.getSubscribers(), contains(redSubscriber, greenSubscriber));
  }

  @Test
  public void shouldNotSubscribeToDataReceiverTwoOrMoreTimes() {
    dataReceiver.subscribe(redSubscriber);
    dataReceiver.subscribe(redSubscriber);

    assertThat(dataReceiver.getSubscribers(), hasSize(1));
  }

  @Test
  public void shouldUnsubscribeFromDataReceiver() {
    dataReceiver.subscribe(redSubscriber);
    dataReceiver.subscribe(greenSubscriber);

    assertThat(dataReceiver.getSubscribers(), contains(redSubscriber, greenSubscriber));

    dataReceiver.unsubscribe(redSubscriber);

    assertThat(dataReceiver.getSubscribers(), contains(greenSubscriber));
  }

  @Test
  public void shouldNotifySubscribers() {
    final byte[] firstDataPack = {'f', 'i', 'r', 's', 't', ' ', 'p', 'a', 'c', 'k'};
    final byte[] secondDataPack = {'s', 'e', 'c', 'o', 'n', 'd', ' ', 'p', 'a', 'c', 'k'};

    dataReceiver.subscribe(redSubscriber);
    dataReceiver.notifySubscribers(firstDataPack);

    dataReceiver.subscribe(greenSubscriber);
    dataReceiver.notifySubscribers(secondDataPack);

    assertThat(redSubscriber.getNotifications(), contains(firstDataPack, secondDataPack));
    assertThat(greenSubscriber.getNotifications(), contains(secondDataPack));
  }

  @Test
  public void shouldProperlyWorkIfThereAreNoAnySubscribers() {
    dataReceiver.notifySubscribers(new byte[] {});
  }

}
