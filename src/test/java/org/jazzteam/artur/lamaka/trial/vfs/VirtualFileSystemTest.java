package org.jazzteam.artur.lamaka.trial.vfs;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.codepoetics.protonpack.StreamUtils;
import java.util.List;
import java.util.function.BiFunction;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

public class VirtualFileSystemTest {

  private Node rootFolder;
  private Node file3;
  private Node file4;
  private Node file5;
  private Node folder222;
  private Node folder333;
  private Node folder444;
  private Node folder555;
  private Node file1;
  private Node file2;

  // 111
  // -- 222
  //    -- 444
  //       -- 1.doc
  //       -- 2.doc
  //    -- 555
  // -- 333
  //       -- 3.doc
  //       -- 4.doc
  //       -- 5.doc
  @Before
  public void setUp() {
    this.rootFolder = new Node("111");
    this.folder222 = new Node("222");
    this.folder333 = new Node("333");
    this.folder444 = new Node("444");
    this.folder555 = new Node("555");
    this.file1 = new Node("1.doc");
    this.file2 = new Node("2.doc");
    this.file3 = new Node("3.doc");
    this.file4 = new Node("4.doc");
    this.file5 = new Node("5.doc");

    rootFolder.insertChild(folder222);
    rootFolder.insertChild(folder333);
    folder222.insertChild(folder444);
    folder222.insertChild(folder555);
    folder444.insertChild(file1);
    folder444.insertChild(file2);
    folder333.insertChild(file3);
    folder333.insertChild(file4);
    folder333.insertChild(file5);

  }


  // 111
  // -- 222
  //    -- 444
  //       -- 1.doc
  //       -- 2.doc
  //    -- 555
  //       -- 3.doc
  //       -- 4.doc
  // -- 333
  @Test
  // TODO: Should be refactored. Keep it only because it's a trial task template
  public void originalTest() {

    file3.moveTo(folder555);
    file4.moveTo(folder555);
    folder333.deleteChild(file5);

    assertThat(folder333.getChildren(), hasSize(0));
    assertThat(folder555.getChildren(), hasSize(2));
    assertThat(folder555.getChildren(), contains(file3, file4));

    assertThat(folder222, equalTo(findCommonAncestor(folder444, folder555)));
  }

  @Test
  public void shouldInsertChild() {
    rootFolder.insertChild(file1);
    assertThat(rootFolder.getChildren().contains(file1), is(true));
    assertThat(file1.getParent(), is(rootFolder));
  }

  @Test
  public void shouldOverwriteNodeWithTheSameName() {
    final Node updatedNode = new Node("1.doc");
    folder444.insertChild(updatedNode);
    assertThat(folder444.getChildren().contains(file1), is(false));
    assertThat(folder444.getChildren().contains(updatedNode), is(true));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldNotInsertNodeIfNodeNameIsNull() {
    rootFolder.insertChild(new Node(null));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldNotInsertNodeIfNodeNameIsBlank() {
    rootFolder.insertChild(new Node("    "));
  }

  @Test
  public void shouldDeleteChild() {
    rootFolder.deleteChild(folder222);
    rootFolder.deleteChild(folder333);
    assertThat(rootFolder.getChildren(), hasSize(0));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionIfTryToDeleteNotExistChild() {
    folder333.deleteChild(file2);
  }

  @Test
  public void shouldMoveNodeWithAllChildren() {
    folder444.moveTo(rootFolder);
    assertThat(rootFolder.getChildren().contains(folder444), is(true));
    assertThat(folder222.getChildren().contains(folder444), is(false));
    assertThat(folder444.getChildren(), contains(file1, file2));
  }

  @Test
  public void shouldOverwriteFileWhenMove() {
    final Node duplicatedNode = new Node("1.doc");
    folder555.insertChild(duplicatedNode);
    duplicatedNode.moveTo(folder444);
    assertThat(file1.getParent(), is(folder444));
    assertThat(folder444.getChildren().contains(file1), is(false));
    assertThat(folder444.getChildren().contains(duplicatedNode), is(true));
  }

  @Test
  public void shouldCalculateAbsolutePathOfNode() {
    assertThat(rootFolder.getPath(), contains(rootFolder));
    assertThat(file1.getPath(), contains(rootFolder, folder222, folder444, file1));
  }

  private static Node findCommonAncestor(Node firstNode, Node secondNode) {

    final List<Node> firstPath = firstNode.getPath();
    final List<Node> secondPath = secondNode.getPath();

    return StreamUtils
        .zip(firstPath.stream(), secondPath.stream(), (BiFunction<Node, Node, Pair>) Pair::new)
        .filter(pair -> pair.getKey() == pair.getValue())
        .reduce((one, two) -> two)
        .map(pair -> (Node) pair.getValue())
        .orElseThrow(() -> new IllegalArgumentException("Common ancestor is not found"));

  }
}